import org.junit.Assert;
import org.junit.Test;

import java.io.*;

/**
 * Created by kit_1414 on 17-Jun-16.
 */
public class PingMessageTest {

    public static void info(String msg) {
        System.out.println("INFO:" + msg);
    }
    public static void error(String msg) {
        System.err.println("ERROR:" + msg);
    }



    protected boolean isSame(PingMessage pm1, PingMessage pm2) {
        if (pm1.getPacketSize()!= pm2.getPacketSize()) return false;
        if (pm1.getStopServerRequest()!= pm2.getStopServerRequest()) return false;
        if (pm1.getPacketNumber()!= pm2.getPacketNumber()) return false;
        if (pm1.getPitcherTime()!= pm2.getPitcherTime()) return false;

        if ((pm1.getCatcherTime() == null) != (pm2.getCatcherTime() == null) ) return false;
        if (pm1.getCatcherTime() != null) {
            if (pm1.getCatcherTime().longValue() != pm2.getCatcherTime().longValue()) return false;
        }
        return true;
    }


    public PingMessage createMessage(int i) {
        boolean odd = i % 2 != 0;
        int minus = odd? 1 : -1;
        i*=minus;

        PingMessage pm = new PingMessage();
        //pm.setPacketSize(i*2);
        pm.setStopServerRequest(odd);
        pm.setPacketNumber(i*3);
        pm.setPitcherTime(i*4);
        pm.setCatcherTime(odd ? null : i*5L);

        return pm;
    }

    @Test
    public void testMessage() throws Exception {
        int iMax = 20;
        int sizeMultiple = IConstants.SIZE_MAX / iMax;
        boolean minMessageChecked =  false ;
        boolean maxMessageChecked =  false ;
        for (int i=0; i<= 20; i++) {
            PingMessage pm=createMessage(i);

            int packetSize = i * sizeMultiple;

            if (packetSize  == 0) {
                packetSize = IConstants.SIZE_MIN;
            }
            if (packetSize  > IConstants.SIZE_MAX) {
                packetSize = IConstants.SIZE_MAX;
            }

            if (packetSize == IConstants.SIZE_MAX) {
                maxMessageChecked = true;
            }
            if (packetSize == IConstants.SIZE_MIN) {
                minMessageChecked = true;
            }

            pm.setPacketSize(packetSize);

            byte[] minPacket = pm.classPacket(); // get minimum packet for message

            Assert.assertTrue(minPacket.length <= packetSize);
            Assert.assertTrue(minPacket.length <= IConstants.SIZE_MIN );
            Assert.assertTrue(minPacket.length <= IConstants.SIZE_MAX );

            // write class to stream (array)
            ByteArrayOutputStream baos =  new ByteArrayOutputStream();
            OutputStream outputStream = baos;
            pm.writeClass(outputStream);
            outputStream.close();

            // check result
            byte[] data = baos.toByteArray();
            Assert.assertEquals(pm.getPacketSize(),data.length);
            Assert.assertEquals(pm.getPacketSize(),packetSize);

            boolean okSize = packetSize >= IConstants.SIZE_MIN && packetSize <= IConstants.SIZE_MAX;
            Assert.assertTrue(okSize);

            // read message back
            InputStream inputStream = new ByteArrayInputStream(data);
            PingMessage pm2 = PingMessage.readClass(inputStream);
            Assert.assertTrue(isSame(pm, pm2));

        }

        Assert.assertTrue(minMessageChecked);
        Assert.assertTrue(maxMessageChecked);

    }
    private byte [] writeClass(PingMessage pm) throws IOException{
        ByteArrayOutputStream baos = new ByteArrayOutputStream(pm.getPacketSize());
        OutputStream os = baos;

        pm.writeClass(os);
        os.close();

        return baos.toByteArray();
    }
}
