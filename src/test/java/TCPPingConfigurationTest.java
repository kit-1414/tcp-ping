import org.junit.Assert;
import org.junit.Test;
import org.springframework.expression.ExpressionException;

import java.util.Arrays;

/**
 * Created by kit_1414 on 15-Jun-16.
 */
public class TCPPingConfigurationTest {
    private static final String [][]VALID_CFGS= new String[] [] {
            {"-p","-port","9000","-mps","20","-size","300","compB"},
            {"-p","-port","9000","compB"},
            {"-c","-bind","127.0.0.1","-port","9000"},

    };
    private static final String [][]INVALID_CFGS= new String[] [] {
            {"-c","-p","-port","9000","-mps","20","-size","300","compB"}, // -c && -p both
            {"-port","9000","-mps","20","-size","300","compB"}, // missed -p
            {"-p","-port","9000","-mps","20","-size","3001","compB"}, // wrong size
            {"-p","-port","9000","-mps","20","-size","49","compB"}, // wrong size
            {"-p","-port","9000","-mps","20","-size","30000","compB"}, // wrong size
            {"-p","-port","9000","-mps","20","-size","300","compB","wrongArg"}, // unexpected arg
            {"-p","-port","9000","-mps","20","-size","300"}, // no B name
            {"-p","-port","9000","-mps","-20"," "}, // compB empty
            {"-p","-port","9000","-mps","-20"}, // compB empty
            {"-p","compB"}, //-port missed
            {"-p","-port","9000"}, // -host missed
            {"-p","-port","9000","-mps","-20","compB"}, //negative rate
            {"-bind","127.0.0.1","-port","9000"}, // -c missed
            {"-c","-port","9000"}, // -bind missed
            {"-c","-bind","127.0.0.1"}, // -port missed
            {"-c","-bind","127.0.0.1","-port","9000sss"}, // wrong ort
            {"-c","-bind","127.0.0.1ss","-port","9000"},// wrong IP


    };

    @Test
    public void validConfigurations() throws Exception {
        for (String [] args : VALID_CFGS) {
            TCPPingConfiguration cfg = new TCPPingConfiguration();
            String errorReason = cfg.parseArgs(args);
            Assert.assertNull("arguments "+(Arrays.toString(args))+" must be valid, but error founded" +errorReason,errorReason);
        }

    }
    @Test
    public void invalidConfigurations() {
        for (String [] args : INVALID_CFGS) {
            TCPPingConfiguration cfg = new TCPPingConfiguration();
            error("========= >>>>>> invalid args : " + (Arrays.toString(args)));
            String errorReason=  cfg.parseArgs(args);
            error("error founded: " + errorReason);
            Assert.assertNotNull("Error must be founded for arguments" + (Arrays.toString(args)),errorReason);
        }

    }
    public static void info(String msg) {
        System.out.println("INFO:" + msg);
    }
    public static void error(String msg) {
        System.err.println("ERROR:" + msg);
    }
}

