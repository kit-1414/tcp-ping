import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kit_1414 on 15-Jun-16.
 */
public class IpMatchTest {
    private static final String[] validIp= new String[] {
            "0.0.0.0",
            "000.0.0.0",
            "000.0.0.000",
            "000.0.0.123",
            "127.0.0.1",
            "192.168.10.1"
    };
    private static final String[] invalidIp= new String[] {
            "0.0.0.0000",
            "0000.0.0.0000",
            "0000.0.0.0",
            ".0.0.0",
            "127.0.0.1.",
            ".127.0.0.1",
            ".0.0.1",
            "127A.0.0.1"
    };

    @Test
    public void validIpTest() {
        for (String ip : validIp) {
            Assert.assertTrue(ip + " must be valid address",isIpValid(ip));
        }
    }
    @Test
    public void invalidIpTest() {
        for (String ip : invalidIp) {
            Assert.assertFalse(ip + " must be invalid address",isIpValid(ip));
        }

    }
    private boolean isIpValid(String ip) {
        return TCPPingConfiguration.isIpValid(ip);
    }
}
