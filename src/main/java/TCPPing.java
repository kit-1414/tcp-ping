import org.apache.log4j.Logger;

public class TCPPing {
	private final static Logger LOG = Logger.getLogger(TCPPing.class);

	public static void main(String []args) {

		LOG.info("TCPPing started.");
		TCPPingConfiguration cfg = new TCPPingConfiguration();
		String errorReason =  cfg.parseArgs(args);
		if (errorReason!= null) {
			error("Wrong argument(s) passed: " + errorReason);
			error(TCPPingConfiguration.getUsageDescription());
			System.exit(1);
		}

        if (cfg.isCatcher()) {
            LOG.info("Catcher mode defined.");
            TcpServer srv = new TcpServer();
            srv.init(cfg);
            srv.runServer().start();
        }
        if (cfg.isPitcher()) {
            LOG.info("Pitcher mode defined.");
            TcpPitcherWorker pw = new TcpPitcherWorker();
            pw.init(cfg);
            pw.runPitcher();
        }
	}
	public static void error(String msg) {
		// System.err.println(msg);
        LOG.error(msg);
	}
}