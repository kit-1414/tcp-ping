import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by kit_1414 on 18-Jun-16.
 */
public class StatData {
    public static final String NEW_LINE = "\n";
    public static final String TIME_SEP = ":";

    public static final String HD_TIME = "TIME: ";
    public static final String HD_TOTAL_MSG_NUM = "Total msg number: ";
    public static final String HD_MSG_NUM = "PrevSecond: msg number ";
    public static final String HD_AV_ABA = "PrevSecond: average ABA time (ms) ";
    public static final String HD_MAX_ABA = "PrevSecond: max ABA time (ms) ";
    public static final String HD_AV_AB = "PrevSecond: average AB time (ms) ";
    public static final String HD_AV_BA = "PrevSecond: average BA time (ms) ";

    private long messagesTotal;
    private long messagesInSecond;
    private long sumAbaTimeInSecond;
    private long maxAbaTimeInSecond;
    private long sumAbTimeInSecond;
    private long sumBaTimeInSecond;
    private long abTimeDiff=0;


    public void init(StatData sd) {
        if (sd == null) {
            return;
        }
        messagesTotal=sd.messagesTotal;
    }

    public void processMessage(PingMessage pm, long currentNanoTime) {
        messagesTotal ++;
        messagesInSecond++;

        long abaTime = currentNanoTime - pm.getPitcherTime();
        sumAbaTimeInSecond+=abaTime;
        if (maxAbaTimeInSecond < abaTime) {
            maxAbaTimeInSecond = abaTime;
        }
        sumAbTimeInSecond+=pm.getCatcherTime() - pm.getPitcherTime() + abTimeDiff;
        sumBaTimeInSecond+=currentNanoTime - pm.getCatcherTime() - abTimeDiff;
    }
    public String getStatText() {
        StringBuilder sb = new StringBuilder();
        Calendar gt = new GregorianCalendar();
        int hours = gt.get(Calendar.HOUR_OF_DAY);
        int minutes = gt.get(Calendar.MINUTE);
        int sec = gt.get(Calendar.SECOND);

        sb.append(HD_TIME).append(hours).append(TIME_SEP).append(minutes).append(TIME_SEP).append(sec).append(NEW_LINE);
        sb.append(HD_TOTAL_MSG_NUM).append(messagesTotal).append(NEW_LINE);
        sb.append(HD_MSG_NUM).append(messagesTotal).append(NEW_LINE);
        sb.append(HD_AV_ABA).append(calcAverageAba()).append(NEW_LINE);
        sb.append(HD_MAX_ABA).append(toDouble(getMaxAbaTimeInSecond())).append(NEW_LINE);
        sb.append(HD_AV_AB).append(calcAverageAb()).append(NEW_LINE);
        sb.append(HD_AV_BA).append(calcAverageBa());
        return sb.toString();
    }

    public double calcAverageAba() {
        return getDoubleDivided(sumAbaTimeInSecond,messagesInSecond );
    }
    public double calcAverageAb() {
        return getDoubleDivided(sumAbTimeInSecond,messagesInSecond );
    }
    public double calcAverageBa() {
        return getDoubleDivided(sumBaTimeInSecond,messagesInSecond );
    }
    private double toDouble(long nano) {
        return (double) nano / (double) IConstants.NANO_IN_MS;
    }
    private double getDoubleDivided(long nano, long num) {
        if (num == 0) {
            return -1;
        }
        double d1=nano;
        double d2 = num;
        return (d1/ (double) IConstants.NANO_IN_MS)/d2;
    }

    public long getMessagesTotal() {
        return messagesTotal;
    }

    public void setMessagesTotal(long messagesTotal) {
        this.messagesTotal = messagesTotal;
    }

    public long getMessagesInSecond() {
        return messagesInSecond;
    }

    public void setMessagesInSecond(long messagesInSecond) {
        this.messagesInSecond = messagesInSecond;
    }

    public long getSumAbaTimeInSecond() {
        return sumAbaTimeInSecond;
    }

    public void setSumAbaTimeInSecond(long sumAbaTimeInSecond) {
        this.sumAbaTimeInSecond = sumAbaTimeInSecond;
    }

    public long getMaxAbaTimeInSecond() {
        return maxAbaTimeInSecond;
    }

    public void setMaxAbaTimeInSecond(long maxAbaTimeInSecond) {
        this.maxAbaTimeInSecond = maxAbaTimeInSecond;
    }

    public long getSumAbTimeInSecond() {
        return sumAbTimeInSecond;
    }

    public void setSumAbTimeInSecond(long sumAbTimeInSecond) {
        this.sumAbTimeInSecond = sumAbTimeInSecond;
    }

    public long getSumBaTimeInSecond() {
        return sumBaTimeInSecond;
    }

    public void setSumBaTimeInSecond(long sumBaTimeInSecond) {
        this.sumBaTimeInSecond = sumBaTimeInSecond;
    }

    public long getAbTimeDiff() {
        return abTimeDiff;
    }

    public void setAbTimeDiff(long abTimeDiff) {
        this.abTimeDiff = abTimeDiff;
    }
}
