/**
 * Created by kit_1414 on 18-Jun-16.
 */
public class StatManager {
    private StatData statData;
    private Long startMoment;

    public StatManager() {
        startMoment = System.nanoTime();
        statData = new StatData();
    }

    public StatData onMessage(PingMessage pm) {
        long nanoTime = System.nanoTime();
        StatData result = null;
        if (nanoTime - startMoment > IConstants.NANO_IN_SECOND) {
            this.startMoment = nanoTime;
            StatData sd = new StatData();
            sd.init(this.statData);
            result = this.statData;
            this.statData = sd;
        }
        this.statData.processMessage(pm,nanoTime);
        return result;
    }
}
