import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by kit_1414 on 17-Jun-16.
 */
public class TcpPitcherWorker {
    private final static Logger LOG = Logger.getLogger(TcpPitcherWorker.class);

    private int port;
    private int rate;
    private int size;
    private String serverHost;
    private boolean stopRequest;

    public void init(TCPPingConfiguration cfg) {
        this.port = cfg.getPort();
        this.rate = cfg.getMpsRate();
        this.size = cfg.getSize();
        this.serverHost = cfg.getCatcherHost();
        this.stopRequest = cfg.isStopServer();
    }
    public void runPitcher() {
        LOG.info("Pitcher started.");
        try (Socket s = new Socket(serverHost, port)) {
            InputStream input = s.getInputStream();
            OutputStream output = s.getOutputStream();
            if (this.stopRequest) {
                sendStopRequest(input,output);
            } else {
                printStatistics(input,output);
            }
        } catch (IOException ex) {
            LOG.error(ex);
        }
        LOG.info("Pitcher finished.");
    }
    private void sendStopRequest(InputStream input , OutputStream output) throws IOException {
        PingMessage pm = getDefMessage();
        pm.setStopServerRequest(true);
        pm.writeClass(output);
    }
    private void printStatistics(InputStream input , OutputStream output) throws IOException {
        long counter = 0;
        long nanoDiff  = IConstants.NANO_IN_SECOND/rate;
        StatManager sm = new StatManager();

        while(true) {
            long t1=System.nanoTime();
            PingMessage pmToSend = getDefMessage();
            pmToSend.setPacketNumber(counter++);
            pmToSend.writeClass(output);
            output.flush();

            //LOG.trace("message sent " + pmToSend);

            PingMessage pmBack = PingMessage.readClass(input);
            StatData sd = sm.onMessage(pmBack);
            if (sd!= null) {
                System.out.println(sd.getStatText());
            }

            //LOG.trace("message received " + pmBack);
            while(System.nanoTime() -t1 < nanoDiff);
        }
    }

    private PingMessage getDefMessage() {
        PingMessage pm = new PingMessage();
        pm.setPacketSize(size);
        pm.setPitcherTime(System.nanoTime());
        pm.setStopServerRequest(false);
        return pm;
    }
}
