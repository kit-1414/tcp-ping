import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by kit_1414 on 15-Jun-16.
 */
public class TCPPingConfiguration implements IConstants  {
    private static final Pattern IP_PATTERN = Pattern.compile("(\\d{1,3}\\.){3}\\d{1,3}");

    static final String USAGE_DESCRIPTION =
                "Usage (catcher case): java TCPPing –c –bind <ip_address> –port <port>\n" +
                "Usage (pitcher case): java TCPPing –p –port <port> –mps <mpsRate> –size <size> <catcher-comp-name>\n" +
                "  Example: java TCPPing -c -bind 127.0.0.1 -port 9900\n"+
                "  Example: TCPPing -p -port 9900 -mps 30 -size 1000 [-stopServer] 127.0.0.1";


    @Option(name="-p",usage="pitcher mode")
    private boolean pitcher;

    @Option(name="-c",usage="catcher mode")
    private boolean catcher;

    @Option(name="-port",usage="port number", required = true)
    private Integer port;

    @Option(name="-bind",usage="IP address to bind server" )
    private String bindAddress;

    @Option(name="-mps",usage="messages per seconds. It must be integer positive (>=1)" )
    private Integer mpsRate;

    @Option(name="-size",usage="network packet size. It must be integer be ["+SIZE_MIN+"-"+SIZE_MAX+"]." )
    private Integer size;

    private String catcherHost;

    @Option(name="-stopServer",usage="stop server request. Uses with -p defined." )
    private boolean stopServer;

    // receives other command line parameters than options
    @Argument
    private List<String> arguments = new ArrayList<String>();

    /**
     * The function parse CMD args.
     *
     * return null, if arguments valid, else String with error description.
     *
     * @param args
     * @return error description or null
     */
    public String parseArgs(String[] args) {
        CmdLineParser parser = new CmdLineParser(this);

        try {
            // parse the arguments.
            parser.parseArgument(args);
            if (pitcher) {
                if (mpsRate == null) {
                    mpsRate = RATE_DEF;
                }
                if (size == null) {
                    size = SIZE_DEF;
                }
            }
            if (pitcher && arguments.size() > 0) {
                catcherHost = arguments.remove(0);
            }

            return validateConfiguration();
        } catch (CmdLineException ex) {
            return ex.getMessage();
        }
    }
    private String validateConfiguration() {
        if (!pitcher && !catcher) {
            return "-p or -c must be specified as program option";
        }
        if (pitcher && catcher) {
            return "one option must be defined: -p or -c";
        }
        if (port==null || port > PORT_MAX || port < 1) {
            return  "valid port number must be defined by -port [1,"+PORT_MAX+"], but founded " + port;
        }
        if (pitcher) {
            if (catcherHost ==null || catcherHost.trim().isEmpty()) {
                return "catcher computer name required for pitcher mode, but it's empty";
            }
            if (size < SIZE_MIN || size > SIZE_MAX) {
                return "wrong value founded for -size "+size+", must be in [" + SIZE_MIN +","+SIZE_MAX+"]";
            }
            if (mpsRate < 1 ) {
                return "wrong value founded for -mpsRate , it must be integer positive: " + mpsRate;
            }
        }

        if (catcher) {
            if (bindAddress == null || !isIpValid(bindAddress)) {
                return "valid IP address must be specified by -bind option for catcher mode";
            }
        }
        if (!arguments.isEmpty()) {
            return "Unexpected parameter(s) founded:" + Arrays.toString(arguments.toArray(new String[arguments.size()]));
        }
        return null;
    }
    public static boolean isIpValid(String ipAddress) {
        return  IP_PATTERN.matcher(ipAddress).matches();
    }
    public static String getUsageDescription() {
        return USAGE_DESCRIPTION;
    }

    public boolean isPitcher() {
        return pitcher;
    }

    public void setPitcher(boolean pitcher) {
        this.pitcher = pitcher;
    }

    public boolean isCatcher() {
        return catcher;
    }

    public void setCatcher(boolean catcher) {
        this.catcher = catcher;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getBindAddress() {
        return bindAddress;
    }

    public void setBindAddress(String bindAddress) {
        this.bindAddress = bindAddress;
    }

    public Integer getMpsRate() {
        return mpsRate;
    }

    public void setMpsRate(Integer mpsRate) {
        this.mpsRate = mpsRate;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getCatcherHost() {
        return catcherHost;
    }

    public void setCatcherHost(String catcherHost) {
        this.catcherHost = catcherHost;
    }

    public boolean isStopServer() {
        return stopServer;
    }

    public void setStopServer(boolean stopServer) {
        this.stopServer = stopServer;
    }

    @Override
    public String toString() {
        return "TCPPingConfiguration{" +
                "pitcher=" + pitcher +
                ", catcher=" + catcher +
                ", port=" + port +
                ", bindAddress='" + bindAddress + '\'' +
                ", mpsRate=" + mpsRate +
                ", size=" + size +
                ", catcherHost='" + catcherHost + '\'' +
                ", stopServer=" + stopServer +
                '}';
    }

}

