import java.io.*;

/**
 * Created by kit_1414 on 17-Jun-16.
 */
public class PingMessage /* implements Externalizable ? */ {
    private static final byte[] INT_ARRAY = intToArray(1);
    private static final int INT_ARRAY_SIZE = INT_ARRAY.length;

    //Transient
    private int packetSize;

    private long packetNumber;
    private long pitcherTime;
    private boolean stopServerRequest;
    private Long catcherTime;

    public static PingMessage readClass(InputStream in) throws IOException {
        byte [] ps =  new byte[INT_ARRAY_SIZE];
        int intArraySize = in.read(ps);
        if (intArraySize == -1) {
            // socket closed on another side.
            return null;
        }
        Integer packetSize = intFromArray(ps);
        if (packetSize == null) {
            return null;
        }
        PingMessage pm = new PingMessage();
        pm.packetSize = packetSize.intValue();

        byte []data = new byte[pm.packetSize-INT_ARRAY_SIZE];
        in.read(data);
        pm.bytesToClass(data);
        return pm;
    }

    public void writeClass(OutputStream out) throws IOException {
        byte[] classData = classPacket();

        if (classData.length > packetSize) {
            throw new IllegalStateException("required packet size="+packetSize+" less than minimum packet size=" + classData.length);
        }
        byte[] fullMessage;
        if (classData.length == packetSize) {
            fullMessage=classData;
        } else {
            fullMessage = new byte[packetSize];
            System.arraycopy(classData,0,fullMessage,0,classData.length);
        }
        out.write(fullMessage);
        out.flush();
    }
    public byte[] classPacket() throws IOException {
        byte[] ps = intToArray(packetSize);
        byte[] data = classToBytes();

        byte[] fullMessage = new byte[ps.length + data.length];
        System.arraycopy(ps,0,fullMessage,0,ps.length);
        System.arraycopy(data,0,fullMessage,ps.length,data.length);
        return fullMessage;
    }
    private byte[] classToBytes() throws IOException {
        try (
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(baos)
        ) {
            dos.writeLong(packetNumber);
            dos.writeLong(pitcherTime);
            dos.writeBoolean(stopServerRequest);
            dos.writeBoolean(catcherTime != null);
            dos.writeLong(catcherTime == null ? 0L : catcherTime);
            dos.flush();
            baos.flush();
            return baos.toByteArray();
        }
    }
    private void bytesToClass(byte[] data) throws IOException {
        try (
                DataInputStream dos = new DataInputStream(new ByteArrayInputStream(data));
        ) {
            this.packetNumber = dos.readLong();
            this.pitcherTime = dos.readLong();
            this.stopServerRequest = dos.readBoolean();
            boolean isCatcherTime = dos.readBoolean();
            long ct = dos.readLong();
            if (isCatcherTime) {
                this.catcherTime=ct;
            } else {
                this.catcherTime = null;
            }
        }
    }
    static byte[] intToArray(int i) {
        try (
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(baos)
        )
        {
            dos.writeInt(i);
            dos.flush();
            baos.flush();
            return baos.toByteArray();
        } catch (IOException ex) {
            throw new IllegalStateException("can't write  to array int i=" + i);
        }
    }
    static Integer intFromArray(byte[] arr) throws IOException {
        try (DataInputStream dos = new DataInputStream(new ByteArrayInputStream(arr)))
        {
            return dos.readInt();
        }
    }

    public int getPacketSize() {
        return packetSize;
    }

    public void setPacketSize(int packetSize) {
        this.packetSize = packetSize;
    }

    public long getPacketNumber() {
        return packetNumber;
    }

    public void setPacketNumber(long packetNumber) {
        this.packetNumber = packetNumber;
    }

    public long getPitcherTime() {
        return pitcherTime;
    }

    public void setPitcherTime(long pitcherTime) {
        this.pitcherTime = pitcherTime;
    }

    public boolean getStopServerRequest() {
        return stopServerRequest;
    }

    public void setStopServerRequest(boolean stopServerRequest) {
        this.stopServerRequest = stopServerRequest;
    }

    public Long getCatcherTime() {
        return catcherTime;
    }

    public void setCatcherTime(Long catcherTime) {
        this.catcherTime = catcherTime;
    }

    @Override
    public String toString() {
        return "PingMessage{" +
                "packetSize=" + packetSize +
                ", packetNumber=" + packetNumber +
                ", pitcherTime=" + pitcherTime +
                ", stopServerRequest=" + stopServerRequest +
                ", catcherTime=" + catcherTime +
                '}';
    }
}