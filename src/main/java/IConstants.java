/**
 * Created by kit_1414 on 18-Jun-16.
 */
interface IConstants {
    long NANO_IN_SECOND= 1000000000;
    long NANO_IN_MS= 1000000;

    int SIZE_MIN=50;
    int SIZE_MAX=3000;
    int SIZE_DEF=300;

    int RATE_DEF=1;

    int PORT_MAX=65535;
}
