import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

/**
 * Created by aamelin on 20.07.2016.
 *
 *
 * http://stackoverflow.com/questions/773121/how-can-i-implement-a-threaded-udp-based-server-in-java
 *
 */
public class UdpServer {
    protected UdpServer(int listenerPort, String bindAddress, Runnable onMessageTask) throws IOException {
        DatagramSocket socket = new DatagramSocket();
        InetSocketAddress address = new InetSocketAddress(bindAddress, listenerPort);
        socket.bind(address);
        DatagramPacket wPacket = null;
        byte[] wBuffer = new byte[ 2048 ];
        while (true) {
                wPacket = new DatagramPacket(wBuffer, wBuffer.length);
                socket.receive(wPacket);
                onMessage(wPacket);
        }
    }
    protected void onMessage(DatagramPacket packet) {

    }
}
