import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by kit_1414 on 17-Jun-16.
 */
public class TcpCatcherWorker implements Runnable {
    private final static Logger LOG = Logger.getLogger(TcpCatcherWorker.class);

    private final Socket clientSocket;
    private final TcpServer tcpServer;


    public TcpCatcherWorker(Socket clientSocket, TcpServer tcpServer) {
        this.clientSocket = clientSocket;
        this.tcpServer = tcpServer;
    }


    @Override
    public void run() {
        LOG.trace("Client thread started");
        try (InputStream input = clientSocket.getInputStream();
            OutputStream output = clientSocket.getOutputStream()){
            PingMessage pm = new PingMessage();
            while (pm!= null) {
                pm = PingMessage.readClass(input);
                if (pm!= null) {
                    pm.setCatcherTime(System.nanoTime());
                    pm.writeClass(output);
                    if (pm.getStopServerRequest()) {
                        LOG.trace("stopServer server request founded" + pm);
                        tcpServer.stopServer();
                    }
                } else {
                    LOG.trace("empty message on server catches, client socket closed");
                }
            }
        } catch (IOException ex) {
            LOG.trace("exception happen during client message processing : "+ex,ex);
        }
        LOG.trace("Client thread finished");
    }
}
