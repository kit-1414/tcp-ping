import org.apache.log4j.Logger;

import javax.net.ServerSocketFactory;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by kit_1414 on 16-Jun-16.
 */
public class TcpServer {
    private final static Logger LOG = Logger.getLogger(TcpServer.class);
    private final static int MAX_CONNECTIONS = 10000;

    private int port;
    private String bindIpAddress;
    protected volatile ServerSocket serverSocket = null;

    public void init(TCPPingConfiguration cfg) {
        this.port = cfg.getPort();
        this.bindIpAddress = cfg.getBindAddress();
    }

    public synchronized Thread runServer() {
        if (this.serverSocket!= null) {
            throw new IllegalStateException("try to run server twice.");
        }
        this.serverSocket = openServerSocket();

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                LOG.info(" TcpServer running on port: " + port + ", bindAddress=" + bindIpAddress);
                boolean serverJob = true;
                while (serverJob) {
                    Socket clientSocket;
                    try {
                        clientSocket = TcpServer.this.serverSocket.accept();
                        new Thread(new TcpCatcherWorker(clientSocket, TcpServer.this)).start();
                    } catch (IOException e) {
                        serverJob = false;
                        LOG.info("Server Stopped.");
                    }
                }
                synchronized (TcpServer.this) {
                    TcpServer.this.serverSocket = null;
                }
                LOG.info("Server thread Stopped.");
            }
        });
        return t;
    }


    public synchronized void stopServer() {
        if (this.serverSocket!= null) {
            try {
                this.serverSocket.close();
            } catch (IOException e) {
                LOG.warn("Error closing server", e);
            }
        }
    }

    private ServerSocket openServerSocket() {
        ServerSocketFactory serverSocketFactory = ServerSocketFactory.getDefault();

        try {
            InetAddress[] ipAddr = Inet4Address.getAllByName(bindIpAddress);
            if (ipAddr == null) {
                throw new IllegalStateException("InetAddress.getAllByName() return null for address " + bindIpAddress);
            }
            if (ipAddr.length == 0) {
                throw new IllegalStateException("InetAddress.getAllByName() return empty array for address " + bindIpAddress);
            }
            if (ipAddr.length > 1) {
                LOG.warn("InetAddress.getAllByName() return " + ipAddr.length + " addresses for " + bindIpAddress);
                for (InetAddress addr : ipAddr) {
                    LOG.warn("InetAddress: " + ipAddr.toString());
                }
            }
            ServerSocket srv = serverSocketFactory.createServerSocket(port, MAX_CONNECTIONS, ipAddr[0]);
            return srv;
        } catch (IOException ex) {
            throw new IllegalStateException("Unable to create server on port=" + port + ", ip=" + bindIpAddress,ex);
        }
    }
}